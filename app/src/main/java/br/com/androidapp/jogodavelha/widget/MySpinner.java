package br.com.androidapp.jogodavelha.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by lucas on 22/06/17.
 */

public class MySpinner extends android.support.v7.widget.AppCompatEditText implements View.OnClickListener{

    protected String[] values;
    protected String selectedValue;
    protected OnSelectValue onSelectValue;
    protected boolean isEmptyValue = false;
    protected boolean emptySelected = false;
    public MySpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(this);
        setFocusable(false);
    }

    public void showDialogValues() {
        new MaterialDialog.Builder(getContext())
                .items(values)
                .itemsCallback((materialDialog, view, i, charSequence) -> {
                    if (i == 0 && isEmptyValue) {
                        setText("");
                        emptySelected = true;
                    } else {
                        selectedValue = charSequence.toString();
                        setText(selectedValue);
                        emptySelected = false;
                    }

                    if (onSelectValue != null) {
                        onSelectValue.onSelect(isEmptyValue ? i - 1 : i, charSequence.toString());
                    }
                }).show();
    }

    @Override
    public void onClick(View v) {
        showDialogValues();
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public void setValuesWithEmpty(String[] values) {
        isEmptyValue = true;
        this.values = addValueDefault(values, getHint().toString());
    }

    public String getSelectedValue() {
        return emptySelected ? "" : selectedValue;
    }

    public void setSelectedValue(int intValue){
        this.selectedValue = values[intValue];
        setText(selectedValue);
    }

    public OnSelectValue getOnSelectValue() {
        return onSelectValue;
    }

    public void setOnSelectValue(OnSelectValue onSelectValue) {
        this.onSelectValue = onSelectValue;
    }

    String[] addValueDefault(String[] values, String defaultStr) {
        int vectorLength = values.length;
        String[] newVector = new String[vectorLength + 1];
        for (int i = 0; i < vectorLength; i++) {
            newVector[i + 1] = values[i];
        }
        newVector[0] = defaultStr;
        return newVector;
    }


    public interface OnSelectValue {
        void onSelect(int position, String value);
    }
}
