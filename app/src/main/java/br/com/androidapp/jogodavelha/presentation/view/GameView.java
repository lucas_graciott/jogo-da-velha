package br.com.androidapp.jogodavelha.presentation.view;

import com.arellomobile.mvp.MvpView;

import br.com.androidapp.jogodavelha.ui.adapter.GameAdapter;
import br.com.androidapp.jogodavelha.ui.adapter.holder.GameButonHolder;

public interface GameView extends MvpView {

    void setAdapter(GameAdapter adapter);
    void selectPlayer(int player);
    void shakeButton(int position);
    void setArraySpnLevels(int resArray);
    void setCallbackSpnLevels();
}
