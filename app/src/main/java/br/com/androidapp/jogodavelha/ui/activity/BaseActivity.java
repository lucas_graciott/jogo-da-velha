package br.com.androidapp.jogodavelha.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;

import br.com.androidapp.jogodavelha.R;

/**
 * Created by lucas on 03/10/17.
 */

public class BaseActivity extends MvpAppCompatActivity {
    protected Toolbar toolbar;

    public void setUpToolbar(int  resTitle) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            toolbar.setNavigationIcon(null);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        TextView textTitle = toolbar.findViewById(R.id.txt_title);
        textTitle.setText(resTitle);
        textTitle.setVisibility(View.VISIBLE);
    }


    public void openActivity(Class<?> openActivity) {
        Intent intent = new Intent();
        intent.setClass(this, openActivity);
        startActivity(intent);
    }


}
