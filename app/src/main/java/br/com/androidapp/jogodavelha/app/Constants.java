package br.com.androidapp.jogodavelha.app;

/**
 * Created by lucas on 06/10/17.
 */

public class Constants {
    public class SharedPreferences {
        public static final String PATH = "path_jogo_da_velha";
        public static final String SAVE_GAME = "save-game";
    }
}
