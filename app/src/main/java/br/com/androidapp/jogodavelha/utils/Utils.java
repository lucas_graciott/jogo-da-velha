package br.com.androidapp.jogodavelha.utils;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import br.com.androidapp.jogodavelha.R;

/**
 * Created by lucas on 05/10/17.
 */

public class Utils {
    public static void sendGame(Context context, String game){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, game);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    public static String objectToString(Object obj) {
        try {
            if (obj == null) {
                return "";
            }
            Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
            return gson.toJson(obj);
        } catch (Exception ex) {
            return "";
        }
    }

    public static Object jsonToObject(String obj, Class<?> classModel) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .create();
        return gson.fromJson(obj, classModel);
    }


    public static void showDialog(Context context, String title, String content, MaterialDialog.SingleButtonCallback callback) {
        MaterialDialog.Builder mBuilder = new MaterialDialog.Builder(context)
                .onPositive(callback);
        if (title != null) {
            mBuilder.title(title);
        }
        MaterialDialog simpleDialog = mBuilder.content(content)
                .positiveText(R.string.ok)
                .build();
        if (!TextUtils.isEmpty(title)) {
            simpleDialog.setTitle(title);
        }

        simpleDialog.show();
    }

}
