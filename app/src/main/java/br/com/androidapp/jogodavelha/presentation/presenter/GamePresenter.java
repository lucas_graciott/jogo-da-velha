package br.com.androidapp.jogodavelha.presentation.presenter;


import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;

import br.com.androidapp.jogodavelha.R;
import br.com.androidapp.jogodavelha.app.MyApplication;
import br.com.androidapp.jogodavelha.model.GameModel;
import br.com.androidapp.jogodavelha.presentation.view.GameView;
import br.com.androidapp.jogodavelha.ui.adapter.GameAdapter;
import br.com.androidapp.jogodavelha.ui.adapter.holder.GameButonHolder;
import br.com.androidapp.jogodavelha.utils.AnimationUtils;
import br.com.androidapp.jogodavelha.utils.Utils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.Random;

@InjectViewState
public class GamePresenter extends MvpPresenter<GameView> {
    private GameAdapter adapter;
    private GameModel game;

    public GamePresenter() {
        game = MyApplication.getInstance().getGame();
        getViewState().selectPlayer(game.getCurrentPlayer());
        adapter = new GameAdapter(game);
        getViewState().setAdapter(adapter);
        getViewState().setArraySpnLevels(R.array.levels);
        getViewState().setCallbackSpnLevels();
    }

    public void selectPlayer(int player) {
        if (player != game.getStartPlayer()) {
            newGame();
            game.setStartPlayer(player);
            getViewState().selectPlayer(player);
        }
    }

    public void setLevel(int level) {
        game.setLevel(level);
    }

    public void changePlayer() {
        game.changePlayer();
    }

    private String getCurrentPlayerActions() {
        return game.getCurrentPlayerActions();
    }

    private String getOponentPlayerActions() {
        return game.getOponentPlayerActions();
    }

    private void addActionPlayer1(String action) {
        game.addMovePlayer1(action);
    }

    private void addActionPlayer2(String action) {
        game.addMovePlayer2(action);
    }

    public void move(Context context, ImageView imageView) {
        int value;
        if (game.getCurrentPlayer() == 0) {
            value = R.mipmap.ic_player_one;
            addActionPlayer1((String) imageView.getTag());
        } else {
            value = R.mipmap.ic_player_two;
            addActionPlayer2((String) imageView.getTag());
        }
        AnimationUtils.changeImageWithAnimation(imageView, ContextCompat.getDrawable(context, value));

        changePlayer();
    }

    public void autoPlayerMove(RecyclerView rcvGame) {
        if (game.getLevel() == 0) {
            easyNextMove(rcvGame);
        } else {
            impossibleNextMove(rcvGame);
        }
    }

    private void easyNextMove(RecyclerView rcvGame) {
        new Handler().postDelayed(() -> {
            Integer[] options = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8};
            Random r = new Random();
            GameButonHolder holder = (GameButonHolder) rcvGame
                    .findViewHolderForAdapterPosition(options[r.nextInt(9)]);
            while (!holder.isEnableButton()) {
                holder = (GameButonHolder) rcvGame
                        .findViewHolderForAdapterPosition(options[r.nextInt(9)]);
            }
            holder.autoPlay();
        }, 400);
    }

    private void impossibleNextMove(RecyclerView rcvGame) {
        new Handler().postDelayed(() -> {
            GameButonHolder holder =
                    (GameButonHolder) rcvGame.findViewHolderForAdapterPosition(getNextMovePosition());
            if (holder.isEnableButton()) {
                holder.autoPlay();
                return;
            }
        }, 400);
    }

    private int getNextMovePosition() {
        return tryWinPositionZero();
    }

    //verifica se o jogo terminou.
    // Se sim,  retorna o jogador vencedor, 2 se deu empate e -1 se o jogo não terminou.
    public int verifyFinalGame() {
        if (getOponentPlayerActions().contains("0")) {
            if (getOponentPlayerActions().contains("1") && getOponentPlayerActions().contains("2")) {
                getViewState().shakeButton(0);
                getViewState().shakeButton(1);
                getViewState().shakeButton(2);
                Log.v("Final Game 0: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
            if (getOponentPlayerActions().contains("4") && getOponentPlayerActions().contains("8")) {
                getViewState().shakeButton(0);
                getViewState().shakeButton(4);
                getViewState().shakeButton(8);
                Log.v("Final Game 0: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
            if (getOponentPlayerActions().contains("3") && getOponentPlayerActions().contains("6")) {
                getViewState().shakeButton(0);
                getViewState().shakeButton(3);
                getViewState().shakeButton(6);
                Log.v("Final Game 0: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
        }
        if (getOponentPlayerActions().contains("1")) {
            if (getOponentPlayerActions().contains("4") && getOponentPlayerActions().contains("7")) {
                getViewState().shakeButton(1);
                getViewState().shakeButton(4);
                getViewState().shakeButton(7);
                Log.v("Final Game 1: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
        }
        if (getOponentPlayerActions().contains("2")) {
            if (getOponentPlayerActions().contains("5") && getOponentPlayerActions().contains("8")) {
                getViewState().shakeButton(2);
                getViewState().shakeButton(5);
                getViewState().shakeButton(8);
                Log.v("Final Game 2: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
            if (getOponentPlayerActions().contains("4") && getOponentPlayerActions().contains("6")) {
                getViewState().shakeButton(2);
                getViewState().shakeButton(4);
                getViewState().shakeButton(6);
                Log.v("Final Game 2: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
        }
        if (getOponentPlayerActions().contains("3")) {
            if (getOponentPlayerActions().contains("4") && getOponentPlayerActions().contains("5")) {
                getViewState().shakeButton(3);
                getViewState().shakeButton(4);
                getViewState().shakeButton(5);
                Log.v("Final Game 3: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
        }
        if (getOponentPlayerActions().contains("6")) {
            if (getOponentPlayerActions().contains("7") && getOponentPlayerActions().contains("8")) {
                getViewState().shakeButton(6);
                getViewState().shakeButton(7);
                getViewState().shakeButton(8);
                Log.v("Final Game 6: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
                return game.getCurrentPlayer() == 0 ? 1 : 0;
            }
        }
        if (getCurrentPlayerActions().length() + getOponentPlayerActions().length() == 9) {
            Log.v("Final Game -1: ", " player: velha actions: " + getOponentPlayerActions());
            return 2;
        }
        Log.v("Final Game -1: ", " player: " + (game.getCurrentPlayer() == 0 ? 1 : 0) + " actions: " + getOponentPlayerActions());
        return -1;
    }

    private boolean verifyPositionIsFree(String position) {
        return !getOponentPlayerActions().contains(position) &&
                !getCurrentPlayerActions().contains(position);
    }

    private int tryWinPositionZero() {
        if (getCurrentPlayerActions().contains("0")) {
            if (getCurrentPlayerActions().contains("1") && verifyPositionIsFree("2")) {
                return 2;
            }
            if (getCurrentPlayerActions().contains("2") && verifyPositionIsFree("1")) {
                return 1;
            }
            if (getCurrentPlayerActions().contains("3") && verifyPositionIsFree("6")) {
                return 6;
            }
            if (getCurrentPlayerActions().contains("4") && verifyPositionIsFree("8")) {
                return 8;
            }
            if (getCurrentPlayerActions().contains("6") && verifyPositionIsFree("3")) {
                return 3;
            }
            if (getCurrentPlayerActions().contains("8") && verifyPositionIsFree("4")) {
                return 4;
            }
        }
        return tryWinPositionOne();
    }

    private int tryWinPositionOne() {
        if (getCurrentPlayerActions().contains("1")) {
            if (getCurrentPlayerActions().contains("2") && verifyPositionIsFree("0")) {
                return 0;
            }
            if (getCurrentPlayerActions().contains("4") && verifyPositionIsFree("7")) {
                return 7;
            }
            if (getCurrentPlayerActions().contains("7") && verifyPositionIsFree("4")) {
                return 4;
            }
        }
        return tryWinPositionTwo();
    }

    private int tryWinPositionTwo() {
        if (getCurrentPlayerActions().contains("2")) {
            if (getCurrentPlayerActions().contains("4") && verifyPositionIsFree("6")) {
                return 6;
            }
            if (getCurrentPlayerActions().contains("5") && verifyPositionIsFree("8")) {
                return 8;
            }
            if (getCurrentPlayerActions().contains("8") && verifyPositionIsFree("5")) {
                return 5;
            }
        }
        return tryWinPositionThree();
    }

    private int tryWinPositionThree() {
        if (getCurrentPlayerActions().contains("3")) {
            if (getCurrentPlayerActions().contains("6") && verifyPositionIsFree("0")) {
                return 0;
            }
            if (getCurrentPlayerActions().contains("4") && verifyPositionIsFree("5")) {
                return 5;
            }
            if (getCurrentPlayerActions().contains("5") && verifyPositionIsFree("4")) {
                return 4;
            }
        }
        return tryWinPositionFour();
    }

    private int tryWinPositionFour() {
        if (getCurrentPlayerActions().contains("4")) {
            if (getCurrentPlayerActions().contains("5") && verifyPositionIsFree("3")) return 3;
            if (getCurrentPlayerActions().contains("6") && verifyPositionIsFree("2")) return 2;
            if (getCurrentPlayerActions().contains("7") && verifyPositionIsFree("1")) return 1;
            if (getCurrentPlayerActions().contains("8") && verifyPositionIsFree("0")) return 0;
        }
        return tryWinPositionFive();
    }

    private int tryWinPositionFive() {
        if (getCurrentPlayerActions().contains("5")) {
            if (getCurrentPlayerActions().contains("8") && verifyPositionIsFree("2")) {
                return 2;
            }
        }
        return tryWinPositionSix();
    }

    private int tryWinPositionSix() {
        if (getCurrentPlayerActions().contains("6")) {
            if (getCurrentPlayerActions().contains("7") && verifyPositionIsFree("8")) {
                return 8;
            }
            if (getCurrentPlayerActions().contains("8") && verifyPositionIsFree("7")) {
                return 7;
            }
        }
        return tryWinPositionSeven();
    }

    private int tryWinPositionSeven() {
        if (getCurrentPlayerActions().contains("7")) {
            if (getCurrentPlayerActions().contains("8") && verifyPositionIsFree("6")) {
                return 6;
            }
        }
        return verifyPositionZero();
    }

    private int verifyPositionZero() {
        if (getOponentPlayerActions().contains("0")) {
            if (getOponentPlayerActions().contains("1") && verifyPositionIsFree("2")) {
                return 2;
            }
            if (getOponentPlayerActions().contains("2") && verifyPositionIsFree("1")) {
                return 1;
            }
            if (getOponentPlayerActions().contains("3") && verifyPositionIsFree("6")) {
                return 6;
            }
            if (getOponentPlayerActions().contains("4") && verifyPositionIsFree("8")) {
                return 8;
            }
            if (getOponentPlayerActions().contains("6") && verifyPositionIsFree("3")) {
                return 3;
            }
            if (getOponentPlayerActions().contains("8") && verifyPositionIsFree("4")) {
                return 4;
            }
        }
        return verifyPositionOne();
    }

    private int verifyPositionOne() {
        if (getOponentPlayerActions().contains("1")) {
            if (getOponentPlayerActions().contains("2") && verifyPositionIsFree("0")) {
                return 0;
            }
            if (getOponentPlayerActions().contains("4") && verifyPositionIsFree("7")) {
                return 7;
            }
            if (getOponentPlayerActions().contains("7") && verifyPositionIsFree("4")) {
                return 4;
            }
        }
        return verifyPositionTwo();
    }

    private int verifyPositionTwo() {
        if (getOponentPlayerActions().contains("2")) {
            if (getOponentPlayerActions().contains("4") && verifyPositionIsFree("6")) {
                return 6;
            }
            if (getOponentPlayerActions().contains("5") && verifyPositionIsFree("8")) {
                return 8;
            }
            if (getOponentPlayerActions().contains("8") && verifyPositionIsFree("5")) {
                return 5;
            }
        }
        return verifyPositionThree();
    }

    private int verifyPositionThree() {
        if (getOponentPlayerActions().contains("3")) {
            if (getOponentPlayerActions().contains("6") && verifyPositionIsFree("0")) {
                return 0;
            }
            if (getOponentPlayerActions().contains("4") && verifyPositionIsFree("5")) {
                return 5;
            }
            if (getOponentPlayerActions().contains("5") && verifyPositionIsFree("4")) {
                return 4;
            }
        }
        return verifyPositionFour();
    }

    private int verifyPositionFour() {
        if (getOponentPlayerActions().contains("4")) {
            if (getOponentPlayerActions().contains("5") && verifyPositionIsFree("3")) {
                return 3;
            }
            if (getOponentPlayerActions().contains("6") && verifyPositionIsFree("2")) {
                return 2;
            }
            if (getOponentPlayerActions().contains("7") && verifyPositionIsFree("1")) {
                return 1;
            }
            if (getOponentPlayerActions().contains("8") && verifyPositionIsFree("0")) {
                return 0;
            }
        }
        return verifyPositionFive();
    }

    private int verifyPositionFive() {
        if (getOponentPlayerActions().contains("5")) {
            if (getOponentPlayerActions().contains("8") && verifyPositionIsFree("2")) {
                return 2;
            }
        }
        return verifyPositionSix();
    }

    private int verifyPositionSix() {
        if (getOponentPlayerActions().contains("6")) {
            if (getOponentPlayerActions().contains("7") && verifyPositionIsFree("8")) {
                return 8;
            }
            if (getOponentPlayerActions().contains("8") && verifyPositionIsFree("7")) {
                return 7;
            }
        }
        return verifyPositionSeven();
    }

    private int verifyPositionSeven() {
        if (getOponentPlayerActions().contains("7")) {
            if (getOponentPlayerActions().contains("8") && verifyPositionIsFree("6")) {
                return 6;
            }
        }
        return attackMove();
    }

    private int firstGameMove() {
        //se adversário marcou o centro ou não marcou ainda, marque um canto.
        //Se ele marcar uma borda ou canto, marque o centro
        if (getOponentPlayerActions().length() == 0 || getOponentPlayerActions().contains("4")) {
            Integer[] options = new Integer[]{0, 2, 6, 8};
            Random r = new Random();
            return options[r.nextInt(4)];
        } else {
            return 4;
        }
    }

    private int secondGameMove() {
        if (getOponentPlayerActions().length() == 1) {
            if (getOponentPlayerActions().contains("4")) {
                if (getCurrentPlayerActions().contains("0")) return 8;
                if (getCurrentPlayerActions().contains("8")) return 0;
                if (getCurrentPlayerActions().contains("6")) return 2;
                if (getCurrentPlayerActions().contains("2")) return 6;
            }
            return 4;
        }

        if (getOponentPlayerActions().contains("4")) {
            if (getCurrentPlayerActions().contains("0") && getOponentPlayerActions().contains("8"))
                return 2; //ou 6
            if (getCurrentPlayerActions().contains("8") && getOponentPlayerActions().contains("0"))
                return 2; //ou 6
            if (getCurrentPlayerActions().contains("2") && getOponentPlayerActions().contains("6"))
                return 8; //ou 0
            if (getCurrentPlayerActions().contains("6") && getOponentPlayerActions().contains("2"))
                return 8; //ou 0
        }

        if (getOponentPlayerActions().contains("0")) {
            if (getOponentPlayerActions().contains("8")) {
                return 1;
            }
            return 8;
        }
        if (getOponentPlayerActions().contains("2")) {
            if (getOponentPlayerActions().contains("6")) {
                return 1;
            }
            return 6;
        }
        if (getOponentPlayerActions().contains("6")) {
            if (getOponentPlayerActions().contains("2")) {
                return 1;
            }
            return 2;
        }
        if (getCurrentPlayerActions().contains("0")) {
            return 1;
        }
        if (getOponentPlayerActions().contains("5") && getOponentPlayerActions().contains("7")) {
            return 8;
        }
        return 0;
    }

    private int attackMove() {
        //primeira vez que o computador joga
        if (getCurrentPlayerActions().length() == 0) {
            return firstGameMove();
        }
        //segunda vez que o computador joga
        if (getCurrentPlayerActions().length() == 1) {
            return secondGameMove();
        }

        if (!getCurrentPlayerActions().contains("4") && !getOponentPlayerActions().contains("4")) {
            return 4;
        }

        if (!getCurrentPlayerActions().contains("1") && !getOponentPlayerActions().contains("1")) {
            if (!getCurrentPlayerActions().contains("1")) {
                return 1;
            }
        }
        if (!getCurrentPlayerActions().contains("3") && !getOponentPlayerActions().contains("3")) {
            return 3;
        }
        if (!getCurrentPlayerActions().contains("5") && !getOponentPlayerActions().contains("5")) {
            return 5;
        }
        if (!getCurrentPlayerActions().contains("7") && !getOponentPlayerActions().contains("7")) {
            return 7;
        }
        if (!getCurrentPlayerActions().contains("0") && !getOponentPlayerActions().contains("0")) {
            return 0;
        }
        if (!getCurrentPlayerActions().contains("2") && !getOponentPlayerActions().contains("2")) {
            return 2;
        }
        if (!getCurrentPlayerActions().contains("6") && !getOponentPlayerActions().contains("6")) {
            return 6;
        }
        return 8;
    }

    public void newGame() {
        int level = game.getLevel();
        game = new GameModel(level);
        adapter.setGameModel(game);
        adapter.notifyDataSetChanged();
        GameButonHolder.isEnableGame = true;
        getViewState().selectPlayer(0);
    }

    public void sendGame(Context context) {
        String strGame = "Player 1: " + game.getActionsPlayer1() + "\n Player 2: " + game.getActionsPlayer2();
        Utils.sendGame(context, strGame);
    }

    public void saveGame() {
        MyApplication.getInstance().saveGame(game);
    }
}
