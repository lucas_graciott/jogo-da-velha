package br.com.androidapp.jogodavelha.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.daimajia.androidanimations.library.YoYo;

import br.com.androidapp.jogodavelha.R;
import br.com.androidapp.jogodavelha.presentation.presenter.GamePresenter;
import br.com.androidapp.jogodavelha.presentation.view.GameView;
import br.com.androidapp.jogodavelha.ui.adapter.GameAdapter;
import br.com.androidapp.jogodavelha.ui.adapter.holder.GameButonHolder;
import br.com.androidapp.jogodavelha.utils.AnimationUtils;
import br.com.androidapp.jogodavelha.utils.Utils;
import br.com.androidapp.jogodavelha.widget.MySpinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameActivity extends BaseActivity implements GameView {
    @InjectPresenter
    GamePresenter mGamePresenter;
    @BindView(R.id.rcv_game)
    RecyclerView rcvGame;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_first_player)
    ImageView btnFirstPlayer;
    @BindView(R.id.btn_second_player)
    ImageView btnSecondPlayer;
    @BindView(R.id.container_header)
    LinearLayout containerHeader;
    @BindView(R.id.btn_new_game)
    Button btnNewGame;
    @BindView(R.id.spn_change_level)
    MySpinner spnChangeLevel;

    private YoYo.YoYoString animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        setUpToolbar(R.string.app_name);
    }

    @OnClick({R.id.btn_new_game, R.id.btn_first_player, R.id.btn_second_player})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_new_game:
                if (animation != null) {
                    animation.stop();
                }
                mGamePresenter.newGame();
                break;
            case R.id.btn_first_player:
                mGamePresenter.selectPlayer(0);
                break;
            case R.id.btn_second_player:
                mGamePresenter.selectPlayer(1);
                GameButonHolder.isEnableGame = false;
                mGamePresenter.autoPlayerMove(rcvGame);
                break;
        }
    }

    @Override
    public void setAdapter(GameAdapter adapter) {
        adapter.setOnClickCallback(new GameAdapter.OnClickCallback() {
            @Override
            public void onClickSinglePlayer(ImageView imageView) {
                mGamePresenter.move(GameActivity.this, imageView);
                int winner = mGamePresenter.verifyFinalGame();
                if (winner == -1) {
                    mGamePresenter.autoPlayerMove(rcvGame);
                } else if (winner == 2) {
                    GameButonHolder.isEnableGame = false;
                    Toast.makeText(GameActivity.this, "Deu velha", Toast.LENGTH_SHORT).show();
                } else {
                    GameButonHolder.isEnableGame = false;
//                    showDialogFinish(winner+1);
//                    Toast.makeText(GameActivity.this, "Click Single Player | vencedor: " + winner, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onClickAutoPlayer(ImageView imageView) {
                mGamePresenter.move(GameActivity.this, imageView);
                int winner = mGamePresenter.verifyFinalGame();
                if (winner != -1) {
                    GameButonHolder.isEnableGame = false;
//                    showDialogFinish(winner+1);
                } else if (winner == 2) {
                    GameButonHolder.isEnableGame = false;
                    Toast.makeText(GameActivity.this, "Deu velha", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onClickTwoPlayers(TextView imageView) {

            }
        });
        rcvGame.setLayoutManager(new GridLayoutManager(this, 3));
        rcvGame.setAdapter(adapter);
    }

    @Override
    public void selectPlayer(int player) {
        if (player == 0) {
            btnFirstPlayer.setSelected(true);
            btnSecondPlayer.setSelected(false);
        } else {
            btnFirstPlayer.setSelected(false);
            btnSecondPlayer.setSelected(true);
        }
    }

    @Override
    public void shakeButton(int position) {
       animation =  AnimationUtils.shake(rcvGame.findViewHolderForAdapterPosition(position).itemView);
    }

    @Override
    public void setArraySpnLevels(int resArray) {
        String[] levels = getResources().getStringArray(resArray);
        spnChangeLevel.setValues(levels);
        spnChangeLevel.setText(getString(R.string.level_x, levels[0]));
    }

    @Override
    public void setCallbackSpnLevels() {
        spnChangeLevel.setOnSelectValue((position, value) -> {
            mGamePresenter.setLevel(position);
            spnChangeLevel.setText(getString(R.string.level_x, value));
        });
    }

    @Override
    public void finish() {
        mGamePresenter.saveGame();
        super.finish();
    }

    private void showDialogFinish(int winner) {
        Utils.showDialog(this,
                getString(R.string.final_game),
                getString(R.string.player_win, winner),
                (dialog, which) -> mGamePresenter.newGame());
    }
}
