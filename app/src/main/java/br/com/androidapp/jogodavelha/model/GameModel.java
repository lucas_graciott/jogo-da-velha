package br.com.androidapp.jogodavelha.model;

/**
 * Created by lucas on 06/10/17.
 */

public class GameModel {
    private String actionsPlayer1;
    private String actionsPlayer2;
    private int level;
    private int startPlayer;
    private int currentPlayer;

    public GameModel(int level) {
        this.actionsPlayer1 = "";
        this.actionsPlayer2 = "";
        this.currentPlayer = 0;
        this.startPlayer = 0;
        this.level = level;
    }

    public void addMovePlayer1(String move) {
        actionsPlayer1 = actionsPlayer1.concat(move);
    }

    public void addMovePlayer2(String move) {
        actionsPlayer2 = actionsPlayer2.concat(move);
    }

    public String getCurrentPlayerActions(){
        return currentPlayer == 0 ? actionsPlayer1 : actionsPlayer2;
    }

    public String getOponentPlayerActions(){
        return currentPlayer == 0 ? actionsPlayer2 : actionsPlayer1;
    }

    public void changePlayer(){
        currentPlayer = currentPlayer == 0 ? 1 : 0;
    }

    public String getActionsPlayer1() {
        return actionsPlayer1;
    }

    public void setActionsPlayer1(String actionsPlayer1) {
        this.actionsPlayer1 = actionsPlayer1;
    }

    public String getActionsPlayer2() {
        return actionsPlayer2;
    }

    public void setActionsPlayer2(String actionsPlayer2) {
        this.actionsPlayer2 = actionsPlayer2;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public int getStartPlayer() {
        return startPlayer;
    }

    public void setStartPlayer(int startPlayer) {
        this.startPlayer = startPlayer;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
