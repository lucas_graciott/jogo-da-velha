package br.com.androidapp.jogodavelha.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import br.com.androidapp.jogodavelha.R;
import br.com.androidapp.jogodavelha.presentation.view.SplashView;
import br.com.androidapp.jogodavelha.presentation.presenter.SplashPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;

public class SplashActivity extends BaseActivity implements SplashView {
    @InjectPresenter
    SplashPresenter mSplashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}
