package br.com.androidapp.jogodavelha.presentation.view;

import com.arellomobile.mvp.MvpView;

public interface SplashView extends MvpView {
    void openActivity(Class<?> openActivity);
    void finish();
}
