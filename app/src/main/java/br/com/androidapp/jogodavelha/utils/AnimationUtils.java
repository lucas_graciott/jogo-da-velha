package br.com.androidapp.jogodavelha.utils;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

/**
 * Created by lucas on 03/10/17.
 */

public class AnimationUtils {

    public static void fadeIn(View view) {
        if (view != null) {
            if (view.getVisibility() == View.VISIBLE) return;
            YoYo.with(Techniques.FadeIn)
                    .duration(400)
                    .playOn(view);
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void fadeOut(final View view) {
        if (view != null) {
            if (view.getVisibility() == View.GONE) return;
            YoYo.with(Techniques.FadeOut)
                    .duration(400)
                    .playOn(view);
            Handler handler = new Handler();
            handler.postDelayed(() ->
                    view.setVisibility(View.GONE), 410);
        }
    }

    public static void changeImageWithAnimation(ImageView imageView, Drawable drawable){
        try {
            fadeOut(imageView);
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                fadeIn(imageView);
                imageView.setImageDrawable(drawable);
            }, 410);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static YoYo.YoYoString shake(View view) {
        if (view != null) {
            return YoYo.with(Techniques.Pulse)
                    .duration(80)
                    .repeat(10)
                    .playOn(view);
        }
        return null;
    }

}
