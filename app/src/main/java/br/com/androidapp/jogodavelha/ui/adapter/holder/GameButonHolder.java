package br.com.androidapp.jogodavelha.ui.adapter.holder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.androidapp.jogodavelha.R;
import br.com.androidapp.jogodavelha.ui.adapter.GameAdapter;
import br.com.androidapp.jogodavelha.utils.AnimationUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lucas on 03/10/17.
 */

public class GameButonHolder extends RecyclerView.ViewHolder {
    public static boolean isEnableGame = true;

    @BindView(R.id.img_game_button)
    ImageView imgGameButton;
    private Context context;
    private GameAdapter.OnClickCallback callback;
    private boolean isEnableButton;

    public GameButonHolder(Context context, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void initHolder(int drawableResource, String position, GameAdapter.OnClickCallback callback) {
        this.callback = callback;
        imgGameButton.setTag(position);
        isEnableButton = drawableResource == R.mipmap.ic_launcher;
        imgGameButton.setImageDrawable(ContextCompat.getDrawable(context, drawableResource));
    }

    @OnClick(R.id.img_game_button)
    public void clickImage() {
        if (isEnableGame && isEnableButton) {
            isEnableGame = false;
            isEnableButton = false;
            callback.onClickSinglePlayer(imgGameButton);
        }
    }

    public void autoPlay() {
        if (isEnableButton) {
            isEnableGame = true;
            isEnableButton = false;
            callback.onClickAutoPlayer(imgGameButton);
        }
    }

    public boolean isEnableButton() {
        return isEnableButton;
    }

}
