package br.com.androidapp.jogodavelha.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.androidapp.jogodavelha.R;
import br.com.androidapp.jogodavelha.model.GameModel;
import br.com.androidapp.jogodavelha.ui.adapter.holder.GameButonHolder;
import br.com.androidapp.jogodavelha.utils.AnimationUtils;

/**
 * Created by lucas on 03/10/17.
 */

public class GameAdapter extends RecyclerView.Adapter<GameButonHolder> {

    public interface OnClickCallback {
        void onClickSinglePlayer(ImageView imageView);
        void onClickAutoPlayer(ImageView imageView);
        void onClickTwoPlayers(TextView imageView);
    }

    private OnClickCallback onClickCallback;
    private GameModel gameModel;

    public GameAdapter(GameModel gameModel) {
        this.gameModel = gameModel;
    }

    @Override
    public GameButonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game_button, parent, false);
        return new GameButonHolder(parent.getContext(), view);
    }

    @Override
    public void onBindViewHolder(GameButonHolder holder, int position) {
        int resDrawable = R.mipmap.ic_launcher;
        if (gameModel.getActionsPlayer1().contains(String.valueOf(position))) {
            resDrawable = R.mipmap.ic_player_one;
        } else if (gameModel.getActionsPlayer2().contains(String.valueOf(position))) {
            resDrawable = R.mipmap.ic_player_two;
        }
        holder.initHolder(resDrawable, String.valueOf(position), onClickCallback);
    }

    @Override
    public int getItemCount() {
        return 9;
    }

    public void setOnClickCallback(OnClickCallback onClickCallback) {
        this.onClickCallback = onClickCallback;
    }

    public void setGameModel(GameModel gameModel) {
        this.gameModel = gameModel;
    }
}
