package br.com.androidapp.jogodavelha.presentation.presenter;


import android.os.Handler;

import br.com.androidapp.jogodavelha.presentation.view.SplashView;
import br.com.androidapp.jogodavelha.ui.activity.GameActivity;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class SplashPresenter extends MvpPresenter<SplashView> {
    public SplashPresenter() {
        openActivity();
    }

    private void openActivity() {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            getViewState().finish();
            getViewState().openActivity(GameActivity.class);
        }, 500);
    }
}
