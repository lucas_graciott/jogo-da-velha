package br.com.androidapp.jogodavelha.app;

import android.app.Application;
import android.text.TextUtils;

import br.com.androidapp.jogodavelha.model.GameModel;
import br.com.androidapp.jogodavelha.utils.PreferencesManager;
import br.com.androidapp.jogodavelha.utils.Utils;

/**
 * Created by lucas on 06/10/17.
 */

public class MyApplication extends Application {
    public static PreferencesManager prefs;
    private static MyApplication instance = new MyApplication();
    private GameModel game;

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        PreferencesManager.initializeInstance(this);
        prefs = PreferencesManager.getInstance();
        loadGame();
    }

    private void loadGame() {
        String json = prefs.getString(Constants.SharedPreferences.SAVE_GAME);
        if (!TextUtils.isEmpty(json)) {
            game = (GameModel) Utils.jsonToObject(json, GameModel.class);
        }else {
            game = new GameModel(0);
        }
    }

    public void saveGame(GameModel game) {
        prefs.setValue(Constants.SharedPreferences.SAVE_GAME, Utils.objectToString(game));
        this.game = game;
    }

    public GameModel getGame() {
        return game;
    }
}
